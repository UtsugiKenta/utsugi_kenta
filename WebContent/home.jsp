<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" href="./style.css" type="text/css">
			<!--	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"> -->
		<title>ホーム画面</title>

	</head>
	<body>aaaa
		<div class="main-contents">
			<div class="header">
			<c:if test="${ not empty loginUser }">
				<a href="message">新規投稿</a>
				<c:if test="${ loginUser.branchId == 1 && loginUser.departmentId == 1 }">
					<a href="management">ユーザー管理</a>
				</c:if>
				<a href="logout">ログアウト</a>
			</c:if>
			</div><br /><br />

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
						<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<div class="messages">
				<form action="./" method="get">
					<table class="sort">
						<tr>
							<td><p>日付</p></td>
							<td><input type="date" name="startDate" value="${startDate}" /></td>
							<td><p align="center">～</p></td>
							<td><input type="date" name="endDate" value="${endDate}" /></td>
						</tr>
						<tr>
							<td><p>カテゴリ</p></td>
							<td><input type="text" name="category" value="${categoryText}" /></td>
							<td></td>
							<td><input type="submit" name="sort" value="絞り込み" /></td>
						</tr>
					</table>
				</form>

				<c:forEach items="${messages}" var="message">

					<div class="message">
						<table>
							 <tr>
								<td><label for="name">ユーザー名：</label></td><td><c:out value="${message.name}" /></td>
							</tr>
							<tr>
								<td><label for="title">件名：</label></td><td><c:out value="${message.title}" /></td>
							</tr>
							<tr>
								<td><label for="category">カテゴリ：</label></td><td><c:out value="${message.category}" /></td>
							</tr>
							<tr>
								<td><label for="text">投稿内容：</label></td><td><pre><c:out value="${message.text}" /></pre></td>
							</tr>
							<tr>
								<td><label for="date">投稿日時：</label></td><td><fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></td>
							</tr>
						</table>
					</div>

					<c:if test="${message.userId ==  loginUser.id}">
						<div class="deleteMessage">
							<form action="deleteMessage" method="post">
								<input  type="hidden" name="messageId" value="${message.id}" />
								<input type="submit" value="削除" onclick="return confirm('本当に削除しますか？')"><br><br>
							</form>
						</div>
					</c:if>

					<div class="commtents">
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.messageId}">
								<div class="comment">
									<div class="name">ユーザー名：<c:out value="${comment.name}" /></div>
									<div class="text">コメント内容：<pre><c:out value="${comment.text}" /></pre></div>
									<div class="date">コメント日時：<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								</div><br />

								<c:if test="${comment.userId == loginUser.id}">
									<div class="deleteComment">
										<form action="deleteComment" method="post">
											<input  type="hidden" name="commentId" value="${comment.id}" />
											<input type="submit" value="削除" onclick="return confirm('本当に削除しますか？')" /><br><br>
										</form>
									</div>
								</c:if>
							</c:if>
						</c:forEach>
					</div>

					<div class="comment-area">
						<form action="comment" method="post">
							コメント入力欄<br />
							<input  type="hidden" name="messageId" value="${message.id}" />
							<textarea name="text" cols="70" rows="5" class="comment-box"></textarea><br />
							<input type="submit" value="コメント投稿"><br><br>
						</form>
					</div>

				</c:forEach>
			</div>
			<div class="copyright"> Copyright(c)Utsugi Kenta</div>
		</div>
	</body>
</html>
