<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>ユーザー新規登録</title>

	</head>
	<body>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
			</c:if>

			<form action="signup" method="post"><br />
				<a href="management">ユーザー管理</a> <br /> <br />
				<div align='left'><label for="account">アカウント</label></div>
				<div align='right'><input name="account" id="account" value="${user.account}"/> <br /></div>

				<div align='left'><label for="password">パスワード</label></div>
				<div align='right'> <input name="password" type="password" id="password"/> <br /></div>

				<div align='left'><label for="checkPassword">確認用パスワード</label></div>
				<div align='right'> <input name="checkPassword" type="password" id="checkPassword"/> <br /></div>

				<div align='left'><label for="name">名前</label></div>
				<div align='right'> <input name="name" id="name" value="${user.name}"/> <br /></div>

				<label for="branchId">支社</label>
				<select name="branchId" id="branchId">
					<c:forEach items="${branches}" var="branch">
						<c:choose>
							<c:when test="${ user.branchId == branch.id }">
								<option value="${branch.id}" selected ><c:out value="${branch.name}"></c:out></option>
							</c:when>
							<c:otherwise>
								<option value="${branch.id}"><c:out value="${branch.name}"></c:out></option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select><br>

				<label for="departmentId">部署</label>
				<select name="departmentId" id="departmentId">
					<c:forEach items="${departments}" var="department">
						<c:choose>
							<c:when test="${ user.departmentId == department.id }">
								<option value="${department.id}" selected ><c:out value="${department.name}"></c:out></option>
							</c:when>
							<c:otherwise>
								<option value="${department.id}"><c:out value="${department.name}"></c:out></option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select><br>

				<input type="submit" value="登録" /> <br /> <br />
			</form>

			<div class="copyright">Copyright(c)Utsugi Kenta</div>
		</div>
	</body>
</html>