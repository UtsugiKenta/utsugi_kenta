package utsugi_kenta.bulletin_board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utsugi_kenta.bulletin_board.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

		HttpSession session = httpRequest.getSession();
		User user = (User) session.getAttribute("loginUser");
		String servletPath = httpRequest.getServletPath();

		if((servletPath.equals("/login"))) {
			chain.doFilter(request, response);

		}else if(servletPath.equals("/css/style.css")){
			chain.doFilter(request, response);

		}else if(user == null) {
			List<String> errorMessages = new ArrayList<String>();
			errorMessages.add("ログインされていません。ログインしてください！");
			session.setAttribute("errorMessages", errorMessages);
			httpResponse.sendRedirect("login");
			return;
		}else {
			chain.doFilter(request, response);
		}
	}


	@Override
	public void init(FilterConfig config) {

	}

	@Override
	public void destroy() {

	}
}
