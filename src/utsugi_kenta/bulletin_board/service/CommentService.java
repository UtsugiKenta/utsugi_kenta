package utsugi_kenta.bulletin_board.service;

import static utsugi_kenta.bulletin_board.utils.CloseableUtil.*;
import static utsugi_kenta.bulletin_board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import utsugi_kenta.bulletin_board.beans.Comment;
import utsugi_kenta.bulletin_board.beans.UserComment;
import utsugi_kenta.bulletin_board.dao.CommentDao;
import utsugi_kenta.bulletin_board.dao.UserCommentDao;

public class CommentService {

	public void insert(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, comment);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	 public List<UserComment> select() {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();

			List<UserComment> comments = new UserCommentDao().select(connection, LIMIT_NUM);

			commit(connection);
			return comments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void delete(Comment comment) {
		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().delete(connection, comment);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
