package utsugi_kenta.bulletin_board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utsugi_kenta.bulletin_board.beans.Comment;
import utsugi_kenta.bulletin_board.service.CommentService;

@WebServlet(urlPatterns = { "/deleteComment"})
public class DeleteCommentServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		int id = Integer.parseInt(request.getParameter("commentId"));

		Comment comment = new Comment();
		comment.setId(id);

		new CommentService().delete(comment);
		response.sendRedirect("./");
	}
}