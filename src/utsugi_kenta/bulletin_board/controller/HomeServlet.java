package utsugi_kenta.bulletin_board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utsugi_kenta.bulletin_board.beans.UserComment;
import utsugi_kenta.bulletin_board.beans.UserMessage;
import utsugi_kenta.bulletin_board.service.CommentService;
import utsugi_kenta.bulletin_board.service.MessageService;

@WebServlet(urlPatterns = { "/"})
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String startDate = request.getParameter("startDate");
		String endDate = request.getParameter("endDate");
		String category =  request.getParameter("category");

		List<UserMessage> messages = new MessageService().select(startDate, endDate, category);

		request.setAttribute("startDate", startDate);
		request.setAttribute("endDate", endDate);
		request.setAttribute("categoryText", category);
		request.setAttribute("messages", messages);
		List<UserComment> comments = new CommentService().select();
		request.setAttribute("comments", comments);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}
