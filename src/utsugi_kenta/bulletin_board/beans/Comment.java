package utsugi_kenta.bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

	private int id;
	private String text;
	private int userId;
	private int messageId;
	private Date createdDate;
	private Date updatedDate;

	// コメントのid
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	// コメントの投稿内容
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	// コメントを入力したユーザーID
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	// コメントを入力したメッセージのid
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	// コメントが投稿された日時
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	// コメントが更新された日時
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
