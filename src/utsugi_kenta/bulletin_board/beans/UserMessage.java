package utsugi_kenta.bulletin_board.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {

	private int id;
	private String account;
	private String name;
	private String title;
	private String category;
	private int userId;
	private String text;
	private Date createdDate;

	// メッセージのid
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	// ユーザーアカウント
	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	// ユーザーネーム
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// メッセージのタイトル
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	// メッセージの内容
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}

	// メッセージのカテゴリ
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	// メッセージを入力したユーザーID
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	// メッセージが作成された日時
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}