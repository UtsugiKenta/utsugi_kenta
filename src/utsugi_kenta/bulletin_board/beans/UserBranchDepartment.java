package utsugi_kenta.bulletin_board.beans;

import java.io.Serializable;

public class UserBranchDepartment implements Serializable {

	private int userId;
	private String userAccount;
	private String userName;
	private String branchName;
	private String departmentName;
	private String stopStatus;
	private int isStopped;

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getStopStatus() {
		return stopStatus;
	}
	public void setStopStatus(String stopStatus) {
		this.stopStatus = stopStatus;
	}

	public int getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(int isStopped) {
		this.isStopped = isStopped;
	}
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}

}
